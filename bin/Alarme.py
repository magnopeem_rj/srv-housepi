#!/usr/bin/python
#-*- coding: utf-8 -*-

import Funcoes
import thread
import threading
import time
import SensorAlarme
import Base
import datetime

DESLIGADO = -1
NORMAL    =  0
DISPARADO =  1

class Alarme(Base.Base):
    
    def __init__(self, sirene, email):
        self.alarmeLigado = False
        self.panicoLigado = False    
        self.thread       = None
        self.status       = DESLIGADO
        self.sirene       = sirene
        self.email        = email
        
        self.carregarSensores()
        
        self.carregarConfiguracao()
        
    def ligarAlarme(self):
        if self.alarmeLigado == False:
            self.status = NORMAL
            self.alarmeLigado = True
            
            if self.thread <> None:
                self.thread.join()
                
            self.thread = threading.Thread(None, self.__monitorarSensores, None, ())
            self.thread.start()
            
            self.atualizarStatusBanco()
        
    def desligarAlarme(self):
        if self.alarmeLigado == True:
            self.alarmeLigado = False
            self.sirene.desligar()
            
            if self.usarSirene == 1:
                if self.status == DISPARADO:
                    time.sleep(0.5)    
            
                self.sirene.ligar()
                time.sleep(0.2)
                self.sirene.desligar()
                time.sleep(0.5)
                self.sirene.ligar()
                time.sleep(0.2)
                self.sirene.desligar()
        
            self.status = DESLIGADO
            self.atualizarStatusBanco()
        
    def ligarPanico(self):
        self.sirene.ligar()
        self.panicoLigado = True
        self.atualizarStatusBanco()
        
    def desligarPanico(self):
        if self.status <> 1:
            self.sirene.desligar()
        
        self.panicoLigado = False
        self.atualizarStatusBanco()
    
    def atualizarStatusBanco(self):
       sql = "update ConfiguracaoAlarme set StatusAlarme = {statusAlarme}, StatusPanico = {statusPanico}"
       sql = sql.format(statusAlarme = int(self.alarmeLigado), statusPanico = int(self.panicoLigado))
       
       return self.executarComando(sql)
     
    def gravarConfiguracaoBanco(self):
        sql = '''update ConfiguracaoAlarme 
                    set TempoDisparo = {tempo}, 
                        UsarSirene = {usarSirene},
                        EnviarEmail = {enviarEmail},
                        DesligarDisparoConsecutivo = {desligarDisparoConsecutivo}'''
            
        sql = sql.format(tempo = int(self.tempoDisparo), usarSirene = int(self.usarSirene), enviarEmail = int(self.enviarEmail), desligarDisparoConsecutivo = int(self.desligarDisparoConsecutivo))
            
        return self.executarComando(sql)
     
    def carregarSensores(self):
        self.sensores = [];
        
        rows = self.consultarRegistros("select * from SensorAlarme")

        for row in rows:
            sensor = SensorAlarme.SensorAlarme(row["Id"], row["NumeroGPIO"], row["Ativo"], row["Nome"])        
            self.sensores.insert(int(row["Id"]), sensor)    
    
    def carregarConfiguracao(self):
        row = self.consultarRegistro("select * from ConfiguracaoAlarme")
        
        self.tempoDisparo               = row["TempoDisparo"]
        self.usarSirene                 = row["UsarSirene"]
        self.enviarEmail                = row["EnviarEmail"]
        self.desligarDisparoConsecutivo = row["DesligarDisparoConsecutivo"]
        
        if row['StatusAlarme'] == 1:
            self.ligarAlarme()
        
        if row['StatusPanico'] == 1:
            self.ligarPanico()     
    
    def gravarRegistroDisparo(self, idSensorDisparo):
        sql = "insert into DisparoAlarme (IdSensorAlarme, DataHora) values ({idSensor}, '{dataHora}')"
        sql = sql.format(idSensor = idSensorDisparo, dataHora = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
       
        return self.executarComando(sql)        

    def getUltimosDisparos(self):
        return self.consultarRegistros('''select DA.Id, 
                                                 SA.Nome, 
                                                 DA.DataHora 
                                            from DisparoAlarme DA 
                                            join SensorAlarme SA 
                                              on SA.Id = DA.IdSensorAlarme 
                                        order by DA.Id desc
                                           limit 15''')

    def __monitorarSensores(self):
        if self.usarSirene == 1:
            self.sirene.ligar()
            time.sleep(0.2)
            self.sirene.desligar()
        
        disparos =  0
        idSensor = -1
        
        for sensor in self.sensores:
            sensor.configurar()

        while self.alarmeLigado: 
            for sensor in self.sensores:
                if (self.alarmeLigado) and (sensor.ativo == 1):
                    if (sensor.lerStatus() == 0):
                    
                        time.sleep(0.5)
                        
                        #duas leituras para garantir que esta realmente disparado
                        if (self.alarmeLigado) and (sensor.ativo == 1):
                            if (sensor.lerStatus() == 0):
                                
                                if idSensor == sensor.id:
                                    disparos = disparos + 1
                                else:
                                    disparos = 1
                                    idSensor = sensor.id
                                    
                                if (disparos > 3) and (self.desligarDisparoConsecutivo == 1):
                                    disparos = 0
                                    self.desligarAlarme()
                                    break
                                
                                self.status = DISPARADO
                                
                                if self.usarSirene == 1:
                                    self.sirene.ligar()
                                
                                if self.enviarEmail == 1:
                                    self.email.carregarConfiguracao()
                                    self.email.enviar(sensor.id, sensor.nome) 
                                
                                self.gravarRegistroDisparo(sensor.id)
                                
                                tempo = 0
                                
                                while tempo < self.tempoDisparo:
                                    if self.alarmeLigado == False:
                                        break
                                    
                                    time.sleep(1)
                                    tempo = tempo + 1
                                
                                if self.alarmeLigado:
                                    self.status = NORMAL
                                
                                if self.usarSirene == 1:
                                    self.sirene.desligar()
                                
                                break
                            elif idSensor == sensor.id:
                                disparos =  0
                                idSensor = -1
                    elif idSensor == sensor.id:
                        disparos =  0
                        idSensor = -1
            time.sleep(0.05)

    def __done__(self):
        self.desligarPanico()
        self.desligarAlarme()