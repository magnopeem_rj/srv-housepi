#!/usr/bin/python
#-*- coding: utf-8 -*-

import Adafruit_MCP230xx
import Base
import time
import thread
import threading

mcp = None

LIGAR            = 1
DESLIGAR         = 0
STATUS_LIGADO    = 1
STATUS_DESLIGADO = 0

class Rele(Base.Base):
    
    def __init__(self, id, numeroGPIO, status, nome, ativo, listaReles):    
        self.id           = id
        self.numeroGPIO   = numeroGPIO
        self.status       = status
        self.nome         = nome
        self.ativo        = ativo
        self.temporizador = 0
        self.thread       = None
        self.listaReles   = listaReles
            
    def configurar(self):
        mcp.config(self.numeroGPIO, mcp.OUTPUT)
    
    def criarMCP(self):
        criar = True

        for rele in self.listaReles:
            if rele.status == STATUS_LIGADO:
                criar = False
                break

        if criar:
            global mcp 
            mcp = Adafruit_MCP230xx.Adafruit_MCP230XX(address=0x20, num_gpios=16)

        self.configurar()

    def ligar(self, temporizador = 0):
        try:
            self.criarMCP()

            mcp.output(self.numeroGPIO, LIGAR)
            
            self.status = STATUS_LIGADO
            
            if temporizador > 0:
                if self.temporizador == 0:
                    self.temporizador = temporizador
                    self.thread = threading.Thread(None, self.__temporizarRele, None, ())
                    self.thread.start()
                else:
                    self.temporizador = temporizador
            return True
        except:
            return False
            
    def desligar(self):
        try:
            mcp.output(self.numeroGPIO, DESLIGAR)
            
            self.status = STATUS_DESLIGADO
            
            self.temporizador = 0
            
            return True
        except:
            return False
    
    def gravarNomeBanco(self):
        sql = "update Rele set Nome = '{nomeRele}', Ativo = {ativo} where Id = {idRele}".format(nomeRele = self.nome, ativo = self.ativo, idRele = self.id)
        
        return self.executarComando(sql)
    
    def atualizarStatusBanco(self):
        if self.numeroGPIO < 10:
            sql = "update Rele set Status = '{statusRele}' where Id = {idRele}".format(statusRele = self.status, idRele = self.id)
        
            return self.executarComando(sql)
        else:
            return True
    
    def __temporizarRele(self):
        tempoDecorido = 0
        
        while self.status == STATUS_LIGADO:
            time.sleep(1)
            
            tempoDecorido = tempoDecorido + 1
            
            if (self.status == STATUS_LIGADO) and ((tempoDecorido / 60) >= self.temporizador):
                self.desligar()
                self.atualizarStatusBanco()