#!/usr/bin/python
#-*- coding: utf-8 -*-

from xml.etree.ElementTree import Element
import xml.etree.ElementTree as ET
import socket
import thread
import threading
import Funcoes
import Automacao
import signal
import sys
import urllib
import os
import os.path
import ConfigParser

CONFIG = os.path.dirname(os.path.abspath(__file__)) + "/Config.ini"

if __name__ == '__main__': 
    
    def signal_handler(signal, frame):
        print "\nEncerrando aplicativo..."
        
        automacao.finalizarProcessos()
        
        tcp.close;
        sys.exit(0)
        
    def notificarAbertura():
        try:
            serial = Funcoes.getSerial()
            
            urllib.urlopen("http://www.housepi.com.br/autenticacao/?serial=" + serial)
        except:
            print "Erro ao notificar abertura do sistema"
    
    def verificarArquivoConfiguracao():
        if not (os.path.exists(CONFIG)):
            Config = ConfigParser.ConfigParser()

            configFile = open(CONFIG,'w')

            Config.add_section('Dados')
            
            Config.set('Dados','Porta', 2342)
            Config.set('Dados','GPIOSirene', 11)
            Config.set('Dados','GPIODHT', 28)
            Config.set('Dados','CaminhoMJPG', '/home/pi/srv-housepi/bin/mjpg-streamer/mjpg-streamer.sh')
            Config.set('Dados','CaminhoPlaylist', '/home/pi/srv-housepi/playlist')
            Config.set('Dados','CaminhoMusicas', '/home/pi/srv-housepi/musicas/')
            Config.set('Dados','CaminhoVideos', '/home/pi/srv-housepi/videos/')
            Config.set('Dados','CaminhoImagens', '/home/pi/srv-housepi/imagens/')
            
            Config.write(configFile)
            
            configFile.close()
            
    def conectado(con, cliente):    
        while True:
            msg = con.recv(4096)
          
            comando = msg[2:len(msg)]
         
            if not msg: 
                break
            
            if len(comando) > 0:
                try:
                    root = ET.fromstring(comando)
                    
                    print cliente, "Comando recebido: " + root.tag 
            
                    if root.tag <> "Logar":
                        if automacao.validarConexao(cliente) == False:
                            con.send("Voce nao esta logado\n")                    
                            break
 
                    if root.tag == "Logar":
                        automacao.efetuarLogin(root, con, cliente)
                    elif root.tag == "Rele":
                        automacao.controlarRele(root, con)
                    elif root.tag == "Temperatura":
                        automacao.enviarTemperaturaHumidade(con)
                    elif root.tag == "Alarme":
                        automacao.controlarAlarme(root, con)
                    elif root.tag == "Panico":
                        automacao.controlarFuncaoPanico(root, con)           
                    elif root.tag == "StatusRele":
                        automacao.enviarConfiguracaoStatusRele(con)
                    elif root.tag == "StatusAlarme":
                        automacao.enviarConfiguracaoStatusAlarme(con)   
                    elif root.tag == "GravarAgendamento":
                        automacao.gravarAgendamento(root, con)
                    elif root.tag == "EnviarAgendamento":
                        automacao.enviarAgendamento(con)
                    elif root.tag == "RemoverAgendamento":
                        automacao.removerAgendamento(root, con)
                    elif root.tag == "AlterarUsuarioSenha":
                        automacao.alterarUsuarioSenha(root, con)
                    elif root.tag == "AlterarConfiguracaoRele":
                        automacao.alterarConfiguracaoRele(root, con)
                    elif root.tag == "AlterarConfiguracaoEmail":
                        automacao.alterarConfiguracaoEmail(root, con)
                    elif root.tag == "EnviarConfiguracaoEmail":
                        automacao.enviarConfiguracaoEmail(con)
                    elif root.tag == "EnviarConfiguracaoAlarme":
                        automacao.enviarConfiguracaoAlarme(con)
                    elif root.tag == "AlterarConfiguracaoAlarme":
                        automacao.alterarConfiguracaoAlarme(root, con)
                    elif root.tag == "EnviarListaMusica":
                        automacao.enviarListaMusica(con)
                    elif root.tag == "ControlarSomAmbiente":
                        automacao.controlarSomAmbiente(root, con)
                    elif root.tag == "ReiniciarDesligar":
                        automacao.reiniciarDesligarServidor(root, con)
                    elif root.tag == "EnviarUltimosDisparos":
                        automacao.enviarUltimosDisparos(con)
                    elif root.tag == "ConfiguracaoRFID":
                        automacao.configuracaoRFID(root, con) 
                    elif root.tag == "EnviarRFID":
                        automacao.enviarRFID(con)
                    elif root.tag == "ConfiguracaoCamera":
                        automacao.configuracaoCamera(root, con) 
                    elif root.tag == "EnviarCamera":
                        automacao.enviarCamera(con)
                    elif root.tag == "EnviarListaVideo":
                        automacao.enviarListaVideo(con)
                    elif root.tag == "ControlarVideo":
                        automacao.controlarVideo(root, con)
                    elif root.tag == "PortaCamera":
                        automacao.enviarPortaCamera(root, con)
                    elif root.tag == "AlterarParametroConfiguracao":
                        automacao.alterarParametroConfiguracao(root, con)
                    elif root.tag == "EnviarParametroConfiguracao":
                        automacao.enviarParametroConfiguracao(con)
                    else:
                        con.send("Comando nao reconhecido\n")
                except Exception as e: 
                    print "Erro: ", e

                    con.send("Erro\n")
                    
        print cliente, "Conexao encerrada"
                
        automacao.removerConexao(cliente)

        con.close()
        thread.exit()
    
    #inicia o servidor    
    verificarArquivoConfiguracao()
    
    HOST = ""                                       
    PORT = int(Funcoes.lerConfiguracaoIni("Porta")) 
    
    orig = (HOST, PORT)
    
    tcp  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp.bind(orig)
    tcp.listen(1)
    
    automacao = Automacao.Automacao()
    
    signal.signal(signal.SIGINT, signal_handler)
    
    notificarAbertura()
    
    print "Aguardando conexoes... (CTRL + C encerra o aplicativo)"
    
    while True:
       conexao, cliente = tcp.accept()
       thread.start_new_thread(conectado, tuple([conexao, cliente]))
       
    tcp.close()
