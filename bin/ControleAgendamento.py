#!/usr/bin/python
#-*- coding: utf-8 -*-

from datetime import date, datetime
import thread
import threading
import time

class ControleAgendamento(threading.Thread):
    def __init__(self, agendamentos):
        threading.Thread.__init__(self)
        self.name = 'ThreadAgendamento'
        self.__stop_thread_event = threading.Event()
        
        self.listaAgendamento = agendamentos
                
    def stop(self):
        self.__stop_thread_event.set()
        
    def run(self):
        while not self.__stop_thread_event.isSet(): 
            atual     = datetime.now().strftime("%Y%m%d%H%M%S")
            diaAtual  = datetime.now().strftime("%Y%m%d") 
            horaAtual = datetime.now().strftime("%H%M%S")
            dthoje    = datetime.today()
            hoje      = dthoje.strftime("%w")
            
            for agendamento in self.listaAgendamento:
                if agendamento.ativo == 1:
                    dtLigar   = datetime.strptime(str(agendamento.dataHoraInicial), "%Y-%m-%d %H:%M:%S")
                    ligar     = dtLigar.strftime("%Y%m%d%H%M%S")
                    diaLigar  = dtLigar.strftime("%Y%m%d")
                    horaLigar = dtLigar.strftime("%H%M%S")
                    
                    dtDesligar   = datetime.strptime(str(agendamento.dataHoraFinal), "%Y-%m-%d %H:%M:%S")
                    desligar     = dtDesligar.strftime("%Y%m%d%H%M%S")
                    diaDesligar  = dtDesligar.strftime("%Y%m%d")
                    horaDesligar = dtDesligar.strftime("%H%M%S")
                                        
                    if len(agendamento.listaDias) > 0:
                        if (diaAtual >= diaLigar) and (diaAtual <= diaDesligar):
                            for dia in agendamento.listaDias:
                                if dia == hoje:
                                    if (horaAtual == horaLigar) and (horaAtual < horaDesligar):
                                        for rele in agendamento.reles:                        
                                            if rele.status == 0:
                                                rele.ligar()    
                                        
                                        if (agendamento.alarme <> None) and (agendamento.alarme.alarmeLigado == False):
                                            agendamento.alarme.ligarAlarme()
               
                                    elif (horaAtual == horaDesligar) and (horaAtual > horaLigar):
                                        for rele in agendamento.reles:                        
                                            if rele.status == 1:
                                                rele.desligar()
                                                rele.atualizarStatusBanco()
                                    
                                        if (agendamento.alarme <> None) and (agendamento.alarme.alarmeLigado == True):
                                            agendamento.alarme.desligarAlarme()
                                            agendamento.alarme.atualizarStatusBanco()
                        
                        if (diaAtual > diaDesligar) or ((diaAtual >= diaDesligar) and (horaAtual > horaDesligar)):
                            agendamento.desativarRegistroBanco()
                        
                    else:
                        if (atual == ligar) and (atual < desligar):
                            for rele in agendamento.reles:                        
                                if rele.status == 0:
                                    rele.ligar()    
                            
                            if (agendamento.alarme <> None) and (agendamento.alarme.alarmeLigado == False):
                                agendamento.alarme.ligarAlarme()
                        
                        elif (atual == desligar) and (atual > ligar):
                            for rele in agendamento.reles:                        
                                if rele.status == 1:
                                    rele.desligar()
                                    rele.atualizarStatusBanco()
                        
                            if (agendamento.alarme <> None) and (agendamento.alarme.alarmeLigado == True):
                                agendamento.alarme.desligarAlarme()
                                agendamento.alarme.atualizarStatusBanco()
                                
                            agendamento.desativarRegistroBanco()
                    
            time.sleep(0.15) 