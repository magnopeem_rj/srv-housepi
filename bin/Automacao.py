#!/usr/bin/python
#-*- coding: utf-8 -*-

import Base
import Usuario
import SomAmbiente
import Email
import Rele
import Alarme
import Camera
import Funcoes
import time
import os
import Agendamento
import ControleAgendamento
import xml.etree.ElementTree as ET
import RFID
import numpy
import Video
import TemperaturaHumidade

from xml.etree.ElementTree import Element

ERRO = "Erro\n"
OK   = "Ok\n"

class Automacao(Base.Base):

    def __init__(self):    
        self.verificarBanco()
        
        self.agendamentos = []
        self.conexoes     = []
        self.reles        = []
        self.tag          = []
        
        self.somAmbiente = SomAmbiente.SomAmbiente()
        self.usuario     = Usuario.Usuario()
        self.video       = Video.Video()
        self.camera      = Camera.Camera(self.usuario)
        self.email       = Email.Email(self.camera)
        
        self.camera.ligar()
        self.carregarReles()
        
        self.alarme = Alarme.Alarme(self.reles[int(Funcoes.lerConfiguracaoIni("GPIOSirene"))], self.email)
        
        self.controleAgendamento = None
        self.carregarAgendamentos()
        self.controleAgendamento = ControleAgendamento.ControleAgendamento(self.agendamentos)
        self.controleAgendamento.start() 
        
        self.RFID = RFID.RFID(self.alarme, numpy.asarray(self.tag))
        self.carregarTag()
        self.RFID.start()

        self.temperaturaHumidade = TemperaturaHumidade.TemperaturaHumidade()
        self.temperaturaHumidade.start()

    def carregarReles(self):
        rows       = self.consultarRegistros("select * from Rele")
        self.reles = []

        for row in rows:
            rele = Rele.Rele(row["Id"], row["NumeroGPIO"], row["Status"], row["Nome"], int(row["Ativo"]), self.reles)        
            
            if rele.status == 1:
                rele.ligar()
                rele.atualizarStatusBanco()
            else:
                rele.desligar()
                rele.atualizarStatusBanco()
                
            self.reles.insert(row["Id"], rele)
    
    def controlarRele(self, root, con):
        acao   = root.find("Acao").text
        numero = root.find("Numero").text
        
        if acao == "Ligar":
            temporizador = int(root.find("Temporizador").text)    
            
            if self.reles[int(numero)].ligar(temporizador):
                con.send(OK)

                self.reles[int(numero)].atualizarStatusBanco()
            else:
                con.send(ERRO)    
        else:
            if self.reles[int(numero)].desligar():
                con.send(OK)

                self.reles[int(numero)].atualizarStatusBanco()
            else:
                con.send(ERRO)
    
    def enviarConfiguracaoStatusRele(self, con):
        root = Element("StatusRele")
        
        for rele in self.reles:
            root.append(Element("Rele" + str(rele.id), Status=str(rele.status), Ativo=str(rele.ativo), Nome=rele.nome.decode('utf-8')))
        
        xmlstr = ET.tostring(root) + "\n"   
        
        con.send(xmlstr)
    
    def alterarConfiguracaoRele(self, root, con):
        try:
            for child in root:
                self.reles[int(child.get("Id"))].nome  = str(child.get("Nome").encode('utf-8')) 
                self.reles[int(child.get("Id"))].ativo = int(child.get("Ativo"))
                self.reles[int(child.get("Id"))].gravarNomeBanco();
            
            con.send(OK)
        except:
            con.send(ERRO)
    
    def efetuarLogin(self, root, con, cliente):
        usuario = root.find("Usuario").text.encode('utf-8')
        senha   = root.find("Senha").text.encode('utf-8')
        
        if self.usuario.validarLogin(usuario, senha):
            self.adicionarConexao(cliente)
            con.send("Logado\n")
        else:
            con.send("NaoLogado\n")
            con.close

    def enviarTemperaturaHumidade(self, con):    
        try:
            self.camera.desligar()

            try:
                con.send(self.temperaturaHumidade.getDados())  
            except:
                con.send(ERRO)
        finally:
            self.camera.ligar()

    def controlarSomAmbiente(self, root, con):
        comando = str(root.find("Comando").text)
        valor   = str(root.find("Valor").text.encode('utf-8'))
        
        if comando == "Play":
            self.somAmbiente.play()
        elif comando == "Pause":
            self.somAmbiente.pause()
        elif comando == "Stop":
            self.somAmbiente.stop()
        elif comando == "AnteriorProxima":
            self.somAmbiente.step(valor)
        elif comando == "Volume":
           self.somAmbiente.volume(valor)
        elif comando == "ReproduzirPorNome":
            self.somAmbiente.playNome(valor)
        elif comando == "TipoAudio":
            self.somAmbiente.tipoAudio(valor)
    
    def enviarListaMusica(self, con):
        lista = self.somAmbiente.getListaMusica()
        
        root = Element("EnviarListaMusica")

        for linha in lista:
            root.append(Element("Musicas", Nome=linha.decode('utf-8')))
                
        xmlstr = ET.tostring(root) + "\n"  
        
        con.send(xmlstr)
    
    def alterarUsuarioSenha(self, root, con):
        usuario = root.find("Usuario").text.encode('utf-8')
        senha   = root.find("Senha").text.encode('utf-8')
        
        if self.usuario.alterarUsuarioSenha(usuario, senha):
            con.send(OK)
        else:
            con.send(ERRO)
    
    def alterarConfiguracaoEmail(self, root, con):
        usuario      = root.find("Usuario").text.encode('utf-8')
        senha        = root.find("Senha").text.encode('utf-8')
        destinatario = root.find("Destinatario").text.encode('utf-8')
        servidor     = root.find("Servidor").text.encode('utf-8')
        porta        = root.find("Porta").text.encode('utf-8')
                
        if self.email.alterarConfiguracao(usuario, destinatario, servidor, porta, senha):
            con.send(OK)
        else:
            con.send(ERRO)
    
    def enviarConfiguracaoEmail(self, con):
        root  = Element("EnviarConfiguracaoEmail")
        
        dados = Element("Dados", Usuario = self.email.remetente.decode('utf-8'), 
                                 Senha = str(self.email.senha).decode('utf-8'), 
                                 Destinatario = self.email.destinatario.decode('utf-8'), 
                                 Servidor = str(self.email.servidorSMTP).decode('utf-8'),
                                 Porta = str(self.email.portaSMTP))
        root.append(dados)
        
        xmlstr = ET.tostring(root) + "\n"       
        
        con.send(xmlstr)
    
    def controlarAlarme(self, root, con):
        acao = root.find("Acao").text
        
        if acao == "Ligar":
            self.alarme.ligarAlarme() 
        else:
            self.alarme.desligarAlarme()
        
        con.send(OK)

    def controlarFuncaoPanico(self, root, con):
        acao = root.find("Acao").text
        
        if acao == "Ligar":
            self.alarme.ligarPanico()
        else:
            self.alarme.desligarPanico()
        
        con.send(OK)
    
    def enviarConfiguracaoStatusAlarme(self, con):
        root   = Element("Alarme")
        status = ""
        
        if self.alarme.status == 1:
            status = "Disparado"
        elif self.alarme.status == 0:
            status = "Normal"
        else:
            status = "Desligado"
        
        root.append(Element("SensorAlarme", Status=status, Ligado=str(int(self.alarme.alarmeLigado))))
        root.append(Element("PanicoAlarme", Ligado=str(int(self.alarme.panicoLigado))))
        
        for sensor in self.alarme.sensores:
            if sensor.ativo == 1:
                root.append(Element("Sensor" + str(sensor.id), Status=str(sensor.lerStatus()), Nome=sensor.nome.decode('utf-8')))

        xmlstr = ET.tostring(root) + "\n"   
        
        con.send(xmlstr)
        
    def enviarConfiguracaoAlarme(self, con):
        root = Element("EnviarConfiguracaoAlarme")
       
        root.append(Element("Geral", TempoDisparo=str(self.alarme.tempoDisparo), 
                                     UsarSirene=str(self.alarme.usarSirene), 
                                     UsarEmail=str(self.alarme.enviarEmail), 
                                     DesligarDisparoConsecutivo=str(self.alarme.desligarDisparoConsecutivo) ))
    
        sensores = Element("Sensores")
        
        for sensor in self.alarme.sensores:
            sensores.append(Element("Sensor" + str(sensor.id), Nome=str(sensor.nome).decode('utf-8'), Ativo=str(sensor.ativo)))
        
        root.append(sensores)
       
        xmlstr = ET.tostring(root) + "\n" 
       
        con.send(xmlstr)
            
    def alterarConfiguracaoAlarme(self, root, con):
        try:
            self.alarme.tempoDisparo               = int(root.find("TempoDisparo").text)
            self.alarme.usarSirene                 = int(root.find("UsarSirene").text)
            self.alarme.enviarEmail                = int(root.find("UsarEmail").text)
            self.alarme.desligarDisparoConsecutivo = int(root.find("DesligarDisparoConsecutivo").text)
            
            self.alarme.gravarConfiguracaoBanco()
       
            sensores  = root.find("Sensores")
        
            for child in sensores:
                id = int(child.get("Id")) 
                
                self.alarme.sensores[id].nome  = child.get("Nome").encode('utf-8')
                self.alarme.sensores[id].ativo = int(child.get("Ativo"))
       
                self.alarme.sensores[id].gravarRegistroBanco()
                
            con.send(OK)
        except Exception, e:
            print "Erro ao alterar configuração do alarme: ", e
        
            con.send(ERRO)
    
    def carregarAgendamentos(self):
        self.agendamentos = []
    
        rows = self.consultarRegistros("select * from Agendamento where Ativo = 1")
    
        for row in rows:
            dias = ''
            
            if int(row["Alarme"]) == 1:
                equipamentos = "-1;"
            else:
                equipamentos = ''
                
            rowsdia = self.consultarRegistros("select Dia from DiaAgendamento where IdAgendamento = {id}".format(id = row["Id"]))
    
            for rowdia in rowsdia:
                dias = dias + str(rowdia["Dia"]) + ";"
            
            rowsequip = self.consultarRegistros("select IdRele from ReleAgendamento where IdAgendamento = {id}".format(id = row["Id"]))
    
            for rowequip in rowsequip:
                equipamentos = equipamentos + str(rowequip["IdRele"]) + ";"
            
            agendamento = Agendamento.Agendamento(row["Id"], row["Nome"], dias, equipamentos, row["DataHoraInicial"], row["DataHoraFinal"], int(row["Ativo"]), self.reles, self.alarme)        
    
            self.agendamentos.insert(row["Id"], agendamento)    
            
    def gravarAgendamento(self, root, con):
        agendamento = Agendamento.Agendamento(0, root.find("Nome").text.encode('utf-8'), root.find("Dias").text, 
                                                 str(root.find("Equipamentos").text), root.find("DataHoraInicial").text, 
                                                 root.find("DataHoraFinal").text, 1, self.reles, self.alarme)
        
        if agendamento.gravarRegistroBanco():
            con.send(OK)
            self.carregarAgendamentos()
            self.controleAgendamento.listaAgendamento = self.agendamentos
        else:
            con.send(ERRO)
    
    def enviarAgendamento(self, con):
        root = Element("EnviarAgendamento")
        
        self.carregarAgendamentos()
        
        for agendamento in self.agendamentos:
            root.append(Element("Agendamento" + str(agendamento.id), 
                                    Id=str(agendamento.id), 
                                    Nome=agendamento.nome.decode('utf-8'), 
                                    DataHoraInicial=str(agendamento.dataHoraInicial), 
                                    DataHoraFinal=str(agendamento.dataHoraFinal), 
                                    Dias=agendamento.dias, 
                                    Equipamentos=agendamento.equipamentos, 
                                    NomeEquipamentos=agendamento.getNomeEquipamento().decode('utf-8')))
                    
        xmlstr = ET.tostring(root) + "\n"   
        
        con.send(xmlstr)
    
    def removerAgendamento(self, root, con):
        for agendamento in self.agendamentos:
            if agendamento.id == int(root.find("Id").text):
                if agendamento.removerRegistroBanco():
                    con.send(OK)
                    
                    self.carregarAgendamentos()
                    self.controleAgendamento.listaAgendamento = self.agendamentos
                    
                    break
                else:
                    con.send(ERRO)
	
    def enviarUltimosDisparos(self, con):
        root = Element("EnviarUltimosDisparos")
        rows = self.alarme.getUltimosDisparos() 
        
        for row in rows:	
            root.append(Element("Disparo", Id=str(row["Id"]),  NomeSensor=row["Nome"].decode('utf-8'), DataHora=str(row["DataHora"])))
                    
        xmlstr = ET.tostring(root) + "\n"   
   
        con.send(xmlstr)
    
    def configuracaoRFID(self, root, con):
        comando = str(root.find("Comando").text)
        valor   = str(root.find("Valor").text.encode('utf-8'))
        
        if comando == "Adicionar":
            sql = "insert into RFID (Tag) values ('{tag}')".format(tag = valor)
            self.executarComando(sql)
        elif comando == "Remover":
            sql = "delete from RFID where Tag = '{tag}'".format(tag = valor)
            self.executarComando(sql)
        
        con.send(OK)
        
        self.carregarTag()
        
    def enviarRFID(self, con):
        root = Element("EnviarRFID")
        rows = self.consultarRegistros("select Tag from RFID") 
        
        for row in rows:    
            root.append(Element("RFID", Tag=str(row["Tag"])))
                    
        xmlstr = ET.tostring(root) + "\n"   
        
        con.send(xmlstr)
    
    def carregarTag(self):
        rows     = self.consultarRegistros("select * from RFID")
        self.tag = []

        for row in rows:
            self.tag.insert(row["Id"], row["Tag"])   
        
        self.RFID.tag = numpy.asarray(self.tag)

    def configuracaoCamera(self, root, con):
        comando = str(root.find("Comando").text)
        
        if comando == "Adicionar":
            Nome      = str(root.find("Nome").text.encode('utf-8'))
            Device    = str(root.find("Device").text.encode('utf-8'))
            Porta     = int(root.find("Porta").text.encode('utf-8'))
            Resolucao = str(root.find("Resolucao").text.encode('utf-8'))
            Frames    = int(root.find("Frames").text.encode('utf-8'))
            YUV       = int(root.find("YUV").text.encode('utf-8'))        

            sql = "insert into Camera (Nome, Device, Porta, Resolucao, Frames, YUV) values ('{nome}', '{device}', {porta}, '{resolucao}', {frames}, {yuv})".format(
                nome = Nome, device = Device, porta = Porta, resolucao = Resolucao, frames = Frames, yuv = YUV)
            
            self.executarComando(sql)
        elif comando == "Remover":
            Nome   = str(root.find("Nome").text.encode('utf-8'))
            
            sql = "delete from Camera where Nome = '{nome}'".format(nome = Nome)
            self.executarComando(sql)
        
        con.send(OK)

        self.camera.desligar()
        self.camera.ligar()
        
    def enviarCamera(self, con):
        root = Element("EnviarCamera")
        rows = self.consultarRegistros("select Nome from Camera") 
        
        for row in rows:    
            root.append(Element("Camera", Nome=str(row["Nome"]).decode('utf-8')))
                    
        xmlstr = ET.tostring(root) + "\n"   
       
        con.send(xmlstr)
    
    def enviarPortaCamera(self, root, con):
        nome = str(root.find("Nome").text.encode('utf-8'))
        
        sql = "select Porta from Camera where Nome = '{nome}'".format(nome = nome)
       
        row = self.consultarRegistro(sql)
        
        con.send(str(row["Porta"]).decode('utf-8') + "\n")        
    
    def controlarVideo(self, root, con):
        comando = str(root.find("Comando").text)
        valor   = str(root.find("Valor").text.encode('utf-8'))
               
        if comando == "Pause":
            self.video.pause()
        elif comando == "Stop":
            self.video.stop()
        elif comando == "Avancar":
            self.video.avancar()
        elif comando == "Retroceder":
            self.video.retroceder()
        elif comando == "ReproduzirPorNome":
            self.video.playNome(valor)    
    
    def enviarListaVideo(self, con):
        lista = self.video.getListaVideo()
        
        root = Element("EnviarListaVideo")

        for linha in lista:
            root.append(Element("Videos", Nome=linha.decode('utf-8')))
                
        xmlstr = ET.tostring(root) + "\n"  
       
        con.send(xmlstr)

    def finalizarProcessos(self):
        if self.alarme.alarmeLigado:
            self.alarme.desligarAlarme()
        
        self.alarme.desligarPanico()
       
        self.controleAgendamento.stop()
       
        self.temperaturaHumidade.stop()

        self.RFID.stop()
        
        for rele in self.reles:    
    		rele.desligar()
    	
        self.camera.desligar()
    
    def reiniciarDesligarServidor(self, root, con):
        acao = root.find("Acao").text
       
        self.finalizarProcessos()
       
        con.send(OK)    
        
        if acao == "Reiniciar":
            os.system("/usr/bin/sudo /sbin/shutdown -r now")
        else:
            os.system("/usr/bin/sudo /sbin/shutdown -h now")

    def alterarParametroConfiguracao(self, root, con):
        try:
            sirene = root.find("GPIOSirene").text
            dht    = root.find("GPIODHT").text
            musica = root.find("DiretorioMusica").text.encode('utf-8')
            video  = root.find("DiretorioVideo").text.encode('utf-8')
            
            self.alarme.sirene = self.reles[int(sirene)]

            Funcoes.alterarConfiguracaoIni("GPIOSirene", sirene)
            Funcoes.alterarConfiguracaoIni("GPIODHT", dht)
            Funcoes.alterarConfiguracaoIni("CaminhoMusicas", musica)
            Funcoes.alterarConfiguracaoIni("CaminhoVideos", video)

            con.send(OK)
        except Exception, e:
            print "Falha ao alterar parametros de configuracao: ", e

            con.send(ERRO)

    def enviarParametroConfiguracao(self, con):
        root  = Element("EnviarParametroConfiguracao")
        
        dados = Element("Dados", GPIOSirene = Funcoes.lerConfiguracaoIni("GPIOSirene"), 
                                 GPIODHT = Funcoes.lerConfiguracaoIni("GPIODHT"),
                                 DiretorioMusica = Funcoes.lerConfiguracaoIni("CaminhoMusicas").decode('utf-8'),
                                 DiretorioVideo = Funcoes.lerConfiguracaoIni("CaminhoVideos").decode('utf-8'))
        root.append(dados)
        
        xmlstr = ET.tostring(root) + "\n"
        
        con.send(xmlstr)

    def removerConexao(self, cliente):
        if cliente in self.conexoes:
            del self.conexoes[self.conexoes.index(cliente)]
            
    def adicionarConexao(self, cliente):
        if not cliente in self.conexoes:
            self.conexoes.insert(len(self.conexoes) + 1, cliente)
            
    def validarConexao(self, cliente):
        return cliente in self.conexoes        